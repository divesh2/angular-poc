import { Component, OnInit } from '@angular/core';
import { Register } from '../common/article';
import {AuthService} from '../services/auth.service'
import { Router } from '@angular/router'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loggedIn:Boolean;
  registerUserData:Register = {'username': '', 'password': ''}
  constructor(private _auth: AuthService,
              private _router: Router) { }

  ngOnInit(): void {
  }

  loginUser() {
    this._auth.loginUser(this.registerUserData)
    .subscribe(
      res => {
        localStorage.setItem('access', res.access)
        localStorage.setItem('refresh', res.refresh)
        this._router.navigate(['/home'])
      },
      err => console.log(err)
    )      
    this.loggedIn = true;
  }

}
