export class Article{
	'id': string;
	'title' : string;
	'image': string;
	'description': string;
	'body': string;
	'author': string;
}

export class Submission{
  'author': string;
  'email': string;
  'phone': number;
  'title': string;
  'description': string;
  'body': string;
  'id': string;
  'image': File;
  // 'article': string;
}


export class Register{
  'username': string;
  'password': string;
}