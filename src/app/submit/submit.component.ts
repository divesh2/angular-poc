import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Submission } from '../common/article';
import {ArticlesService} from '../services/articles.service'
import {Router} from '@angular/router';
import { AuthService } from '../services/auth.service'


@Component({
  selector: 'app-submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.css']
})
export class SubmitComponent implements OnInit {
  submissionForm: FormGroup;
  submission: Submission;
  public valid;
  public articles;
  public id;
  public image;
  public loggedIn;
  @ViewChild('fform') submissionFormDirective;

  formErrors = {
    'author': '',
    'email': '',
    'phone': '',
    'title': '',
    'description': '',
    'body':''
  };

  validationMessages = {
    'author': {
      'required': 'name is required.',
      'minlength': 'name must be at least 2 characters long',
      'maxlength': 'name cannot be more than 25 charcters long'
    },
    'title': {
      'required': 'Title is required.',
      'minlength': 'Title must be at least 2 characters long',
      'maxlength': 'Title cannot be more than 25 charcters long'
    },
    'description': {
      'required': 'Description is required.',
      'minlength': 'Description must be at least 10 characters long',
      'maxlength': 'Description cannot be more than 2500 charcters long'
    },
    'body': {
      'required': 'Description is required.',
      'minlength': 'Description must be at least 10 characters long',
      'maxlength': 'Description cannot be more than 2500 charcters long'
    },
    'phone': {
      'required': 'Phone. number is required.',
      'pattern': 'Phone. number must contain only numbers.'
    },
    'email': {
      'required': 'Email is required.',
      'email': 'Email not in valid format'
    },
    'article': {
      'required': 'Article is required.',
      'minlength': 'Article must be at least 100 characters long',
      'maxlength': 'Article cannot be more than 2500 charcters long'
    }
  };

  constructor(private fb: FormBuilder, private service: ArticlesService,private auth_service: AuthService ,private router: Router) {
    this.createForm();
  }



  ngOnInit() :void {
    this.loggedIn = this.auth_service.loggedIn();
  }

  createForm() {
    this.submissionForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern]],
      title: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(250)]],
      description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(2500)]],
      body: ['', [Validators.required, Validators.minLength(100), Validators.maxLength(2500)]],
      image: null
    });

    this.submissionForm.valueChanges
    .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if(this.submissionForm.status == 'VALID'){
      this.valid = true;
    }
    console.log(this.submissionForm.status)
    if (!this.submissionForm) { return; }
    const form = this.submissionForm;
    for (const field in this.formErrors ) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }

          }
        }
      }
    }
  }

  onChange(event) {
        
        let file = event.target.files[0];
        // formData.append("file", file);
        // console.log(formData)

        this.submissionForm.value['image'] = event.target.files[0];
        console.log(this.submissionForm.value)
        
    }


  onSubmit() {
    this.submission = this.submissionForm.value;
    let formData:FormData = new FormData();  
    const data = this.submission
    for (const key in data) {
      console.log(data[key])
    formData.append(key, data[key]);
    }

    this.service.addArticle(formData).subscribe(data => this.router.navigate(['/article', data]) );
  }
}
 
