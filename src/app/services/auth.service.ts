import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Router } from '@angular/router'
import BASE_API_URL from '../common/base_url'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,private router: Router) { }

  private url_register = BASE_API_URL  +  '/api/register'
  private url_login = BASE_API_URL + '/api/token/'

  public httpOptions = {
    headers: new HttpHeaders().set('Content-Type', 'application/json')
    };


  registerUser(body:any): Observable<any>{
    return this.http.post<any>(this.url_register , body, this.httpOptions)
  }

  loginUser(body:any): Observable<any>{
    return this.http.post<any>(this.url_login , body, this.httpOptions)
  }


  getToken() {
    return localStorage.getItem('access')
  }

  loggedIn() {
    return !!localStorage.getItem('access')    
  }

  logoutUser() {
    localStorage.removeItem('access')
    this.router.navigate(['/login'])
  }

}
