import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import {Article, Submission} from '../common/article'
import { Router } from '@angular/router'
import BASE_API_URL from '../common/base_url'



@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private http: HttpClient,private router: Router) { }
  private url = BASE_API_URL + '/';

  public httpOptions = {
    headers: new HttpHeaders().set('Content-Type', 'application/json')
    };
  

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.url)
  }

  getArticle(id): Observable<Article[]> {
    return this.http.get<Article[]>(this.url + 'article/' + id)
  }

  addArticle(submission:any): Observable<any> {
    // const body=JSON.stringify(submission);
    const body = submission;
    console.log('body:',body)
    return this.http.post<any>(this.url , body)
  }

  deleteArticle(id): Observable<unknown> {
  return this.http.delete(this.url + 'article/'  + id, this.httpOptions)
}


}
