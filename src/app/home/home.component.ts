import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ArticlesService} from '../services/articles.service'
import BASE_API_URL from '../common/base_url'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public data;
  public src = BASE_API_URL;
  constructor(
    private service: ArticlesService, private router: Router
    ) { }

  ngOnInit() {
      this.service.getArticles().subscribe(data => this.data = data);
  }

  gotoid(item){
    this.router.navigate(['/article', item.id])
  }

}
