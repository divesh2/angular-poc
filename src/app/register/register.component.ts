import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service'
import { Router } from '@angular/router'
import { Register } from '../common/article';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerUserData:Register = {'username': '', 'password': ''}
  constructor(private _auth: AuthService,
              private _router: Router) { }

  ngOnInit() {
  }

  registerUser() {
    this._auth.registerUser(this.registerUserData)
    .subscribe(
      res => {
        localStorage.setItem('token', res.token)
        console.log(res)
        this._router.navigate(['/home'])
      },
      err => console.log(err)
    )      
  }


}
