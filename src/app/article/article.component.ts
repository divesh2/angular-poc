import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import {ArticlesService} from '../services/articles.service'
import {AuthService} from '../services/auth.service'
import BASE_API_URL from '../common/base_url'


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  public item;
  public src = BASE_API_URL;
  public loggedIn;

  constructor(private route: ActivatedRoute,private router: Router ,
    private service: ArticlesService, private auth_service: AuthService) { }

  ngOnInit(): void {

    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.service.getArticle(id).subscribe(data => this.item = data);
    this.loggedIn = this.auth_service.loggedIn()
  }

  deleteArt(item){
    this.service.deleteArticle(item.id).subscribe(data=> console.log(data));
    alert('This article has been deleted');
    this.router.navigate(['/home']);
  }

}

